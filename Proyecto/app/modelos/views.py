from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .Forms import FormularioUsuario, FormularioPedido, FormularioRestaurante, FormularioPlato, FormularioPlato_pedido, FormularioLogin
from app.modelos.models import Usuario;
from app.modelos.models import Plato;
from app.modelos.models import Restaurante;
from app.modelos.models import Plato_pedido;
from app.modelos.models import Pedido;

# Create your views here.

## LOGIN ##
def ingresar(request):
	if request.method == 'POST':
		formulario = FormularioLogin(request.POST)
		if formulario.is_valid():
			usuario = request.POST['username']
			password = request.POST['password']
			user = authenticate(username = usuario, password = password)
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponseRedirect(reverse('principal'))
				else:
					print ('usuario desactivado')
			else:
				print ('usuario y/o contraseña no valida')
	else:
		formulario = FormularioLogin()
	context = {
		'f': formulario,
	}
	return render(request, 'login/login.html', context)

def cerrar(request):

	logout(request)

	return HttpResponseRedirect(reverse('autenticar'))

## METODOS

@login_required(login_url = '/login')
def principal(request):
	return render(request, 'principal.html');

@login_required(login_url = '/login')
def listarUsuario(request):
	lista = Usuario.objects.all()
	context = {
		'lista' : lista,
	}
	return render(request, 'Cliente/listarCliente.html', context);

@login_required(login_url = '/login')
def crearUsuario(request):
	usuario = request.user
	formularioUsuario = FormularioUsuario(request.POST)
	#if usuario.groups.filter(name = 'administrativo').exists():
	if request.method == 'POST':
		if formularioUsuario.is_valid():
			datos = formularioUsuario.cleaned_data
			usuario = Usuario();
			usuario.nombre = datos.get('nombre')
			usuario.correo = datos.get('correo')
			usuario.numero_telefono = datos.get('numero_telefono')
			usuario.password = datos.get('password')
			usuario.estado = "Activo"
			usuario.save()

			return redirect(listarUsuario)

	context = {
		'fu': formularioUsuario
	}

	return render(request, 'Cliente/crearCliente.html', context);

@login_required(login_url = '/login')
def listarPedidos(request):
	lista = Pedido.objects.all()
	context = {
		'lista' : lista,
	}
	return render(request, 'Pedido/pedidos.html', context);


@login_required(login_url = '/login')
def listarRestaurante(request):
	lista = Restaurante.objects.all()
	context = {
		'lista' : lista,
	}
	return render(request, 'Restaurante/PrincipalRestaurante.html', context);


@login_required(login_url = '/login')
def crearRestaurante(request):
	usuario = request.user
	formularioRestaurante = FormularioRestaurante(request.POST)	
	if request.method == 'POST':
		if formularioRestaurante.is_valid():
			datos = formularioRestaurante.cleaned_data
			restaurante = Restaurante();
			restaurante.nombre = datos.get('nombre')
			restaurante.horario = datos.get('horario')
			restaurante.calificacion = datos.get('calificacion')			
			restaurante.estado = "Activo"
			restaurante.save()

			return redirect(listarRestaurante)

	context = {
		'fu': formularioRestaurante
	}

	return render(request, 'Restaurante/crearRestaurante.html', context);


@login_required(login_url = '/login')
def listarPlato(request):	
	id = request.GET['restaurante_id']
	try:
		restaurante = Restaurante.objects.get(restaurante_id = id)
	except Restaurante.DoesNotExist:
		return redirect(listarRestaurante)		
	lista = Plato.objects.filter(restaurante_id = restaurante.restaurante_id)
	context = {
		'lista' : lista,
		'id' : id
	}
	return render(request, 'Plato/listarPlatos.html', context);

@login_required(login_url = '/login')
def crearPlato(request):
	id = request.GET['restaurante_id']
	formularioPlato = FormularioPlato(request.POST)	
	if request.method == 'POST':
		if formularioPlato.is_valid():
			datos = formularioPlato.cleaned_data
			plato = Plato();
			plato.nombre = datos.get('nombre')
			plato.costo = datos.get('costo')
			plato.descripcion = datos.get('descripcion')			
			plato.estado = True
			plato.restaurante_id = id
			plato.save()

			return redirect(listarRestaurante)

	context = {
		'fu': formularioPlato
	}

	return render(request, 'Plato/crearPlato.html', context);