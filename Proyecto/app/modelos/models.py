from django.db import models

# Create your models here.
class Usuario (models.Model):
	usuario_id = models.AutoField(primary_key = True)
	nombre = models.CharField(max_length = 20, null = False)	
	correo = models.EmailField(max_length = 50, null = False, unique = True)
	numero_telefono = models.CharField(max_length = 10, null = False)
	password = models.CharField(max_length = 20, null = False)
	estado = models.CharField(max_length = 15, null = False)
	createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
	updatedAt = models.DateField('UpdateddAt', auto_now=True, auto_now_add=False)


class Pedido(models.Model):
	pedido_id = models.AutoField(primary_key = True)	
	estado = models.CharField(max_length = 15, null = False)
	valor_total = models.DecimalField(max_digits = 5, decimal_places = 2, null = False)
	tiempo_estimado = models.IntegerField(null = False)
	extras = models.TextField(max_length = 100)		
	createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
	updatedAt = models.DateField('UpdateddAt', auto_now=True, auto_now_add=False)
	usuario = models.ForeignKey('Usuario', on_delete = models.CASCADE)

class Restaurante(models.Model):
	restaurante_id = models.AutoField(primary_key = True)
	nombre = models.CharField(max_length = 20, null = False)	
	horario = models.CharField(max_length = 50, null = False)
	calificacion = models.DecimalField(max_digits = 5, decimal_places = 2, null = False)
	estado = models.CharField(max_length = 15, null = False)
	createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
	updatedAt = models.DateField('UpdateddAt', auto_now=True, auto_now_add=False)

class Plato(models.Model):
	plato_id = models.AutoField(primary_key = True)
	nombre = models.CharField(max_length = 20, null = False)
	costo = models.DecimalField(max_digits = 5, decimal_places = 2, null = False)
	descripcion = models.TextField(max_length = 100, null = False)	
	estado = models.BooleanField(default = True)
	restaurante = models.ForeignKey('Restaurante', on_delete = models.CASCADE)
	createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
	updatedAt = models.DateField('UpdateddAt', auto_now=True, auto_now_add=False)
	

class Plato_pedido(models.Model):
	plato_pedido_id = models.AutoField(primary_key = True)
	cantidad = models.IntegerField(null = False)
	plato = models.ForeignKey('Plato', on_delete = models.CASCADE)
	pedido = models.ForeignKey('Pedido', on_delete = models.CASCADE)
	createdAt = models.DateField('CreatedAt', auto_now=True, auto_now_add=False)
	updatedAt = models.DateField('UpdateddAt', auto_now=True, auto_now_add=False)