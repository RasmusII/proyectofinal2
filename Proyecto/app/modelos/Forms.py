from django import forms;
from app.modelos.models import Usuario;
from app.modelos.models import Plato;
from app.modelos.models import Restaurante;
from app.modelos.models import Plato_pedido;
from app.modelos.models import Pedido;

class FormularioUsuario(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ["nombre", "correo", "numero_telefono", "password"]

class FormularioPedido(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = ["valor_total", "tiempo_estimado", "extras"]

class FormularioRestaurante(forms.ModelForm):
    class Meta:
        model = Restaurante
        fields = ["nombre", "horario", "calificacion"]

class FormularioPlato_pedido(forms.ModelForm):
    class Meta:
        model = Plato_pedido
        fields = ["cantidad"]

class FormularioPlato(forms.ModelForm):
    class Meta:
        model = Plato
        fields = ["nombre", "costo", "descripcion"]

class FormularioLogin(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget = forms.PasswordInput())