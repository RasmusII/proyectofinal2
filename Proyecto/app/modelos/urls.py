from django.urls import path

from . import views

urlpatterns = [
	path('', views.principal, name = 'modelos'),
    path('login', views.ingresar, name='autenticar'),	
	path('logout', views.cerrar, name='cerrar_sesion'),	
	path('principal', views.principal, name = 'principal'),
	path('listarUsuario', views.listarUsuario, name = 'listarUsuario'),
	path('crearUsuario', views.crearUsuario, name = 'crearUsuario'),
	path('pedidos', views.listarPedidos, name='pedidos'),
	path('restaurante', views.listarRestaurante, name = 'restaurante'),
	path('crearRestaurante', views.crearRestaurante, name= 'crearRestaurante'),
	path('listarPlato', views.listarPlato, name= 'listarPlato'),
	path('crearPlato', views.crearPlato, name= 'crearPlato'),
]