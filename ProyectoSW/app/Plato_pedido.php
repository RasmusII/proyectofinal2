<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Plato_pedido extends Model
{
    
    protected $table = 'modelos_plato_pedido';
    protected $primaryKey = 'plato_pedido_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plato_pedido_id', 'cantidad','plato', 'pedido', 'createdAt', 'updatedAt'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
