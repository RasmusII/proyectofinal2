<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Plato extends Model
{
    
    protected $table = 'modelos_plato';
    protected $primaryKey = 'plato_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plato_id', 'nombre', 'costo', 'descripcion', 'estado', 'restaurante','createdAt', 'updatedAt'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
