<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pedido extends Model
{
    
    protected $table = 'modelos_pedido';
    protected $primaryKey = 'pedido_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id', 'estado', 'valor_total', 'tiempo_estimado', 'extras', 'createdAt', 'updatedAt', 'usuario_id'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
