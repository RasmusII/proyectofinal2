<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Usuario extends Model
{
    
    protected $table = 'modelos_usuario';
    protected $primaryKey = 'usuario_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario_id', 'nombre', 'correo', 'numero_telefono', 'password', 'estado', 'createdAt', 'updatedAt'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
