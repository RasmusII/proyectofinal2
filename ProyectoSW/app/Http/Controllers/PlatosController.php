<?php

namespace App\Http\Controllers;

use App\Plato;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PlatosController extends BaseController
{
    public function all(Request $request){
    	$Plato = Plato::all();
    	return response()->json($Plato, 200);    	
    }

    public function getPlato(Request $request, $nombre){
    	if($request -> isJson()){
    		$Plato = Plato::where('nombre', $nombre) -> get();
    		if(!$Plato -> isEmpty()){
    			$status = true;
    			$info = "Data is listed successfuly";
    		}
    		else{
 		   		$status = false;
    			$info = "Data is not listed successfuly";	
    		}
    		return ResponseBuilder::result($status, $info, $Plato);
    	}
    	else{
    		$status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
    	}    	
    }

    
}