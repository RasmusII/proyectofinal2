<?php

namespace App\Http\Controllers;

use App\Restaurante;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class RestauranteController extends BaseController
{
    public function all(Request $request){
    	$restaurante = Restaurante::all();
    	return response()->json($restaurante, 200);    	
    }

    public function getRestaurante(Request $request, $nombre){
    	if($request -> isJson()){
    		$restaurante = Restaurante::where('nombre', $nombre) -> get();
    		if(!$restaurante -> isEmpty()){
    			$status = true;
    			$info = "Data is listed successfuly";
    		}
    		else{
 		   		$status = false;
    			$info = "Data is not listed successfuly";	
    		}
    		return ResponseBuilder::result($status, $info, $restaurante);
    	}
    	else{
    		$status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
    	}    	
    }

    public function createRestaurante(Request $request){

    	$restaurante = new Restaurante();
    	
    	$restaurante -> nombre = $request->nombre;
        $restaurante -> horario = $request->horario;
        $restaurante -> calificacion = $request->calificacion;        
        $restaurante -> estado = $request->estado;
        $restaurante -> createdAt = Date($aux->format ('Y-m-d'));
        $restaurante -> updatedAt = Date($aux->format ('Y-m-d'));
        $restaurante -> save();
        
    	$info = 'Restaurante creado correctacmente';
    	$status = true;

    	return(ResponseBuilder::result($status, $info, $restaurante));
    }   

    
}