<?php

namespace App\Http\Controllers;

use DateTime;
use App\Pedido;
use App\Plato_pedido;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PedidoController extends BaseController
{
    public function all(Request $request){
    	$Pedido = Pedido::all();
    	return response()->json($Pedido, 200);    	
    }

    public function getPedido(Request $request, $pedido_id){
    	if($request -> isJson()){
    		$Pedido = Pedido::where('pedido_id', $pedido_id) -> get();
    		if(!$Pedido -> isEmpty()){
    			$status = true;
    			$info = "Data is listed successfuly";
    		}
    		else{
 		   		$status = false;
    			$info = "Data is not listed successfuly";	
    		}
    		return ResponseBuilder::result($status, $info, $Pedido);
    	}
    	else{
    		$status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
    	}    	
    }

    public function createPedido(Request $request){

    
        $Pedido = new Pedido();
    	$aux = new DateTime('now');
    	$Pedido -> estado = $request->estado;
        $Pedido -> valor_total = $request->valor_total;
        $Pedido -> tiempo_estimado = $request->tiempo_estimado;        
        $Pedido -> extras = $request->extras;
        $Pedido -> createdAt = Date($aux->format ('Y-m-d'));
        $Pedido -> updatedAt = Date($aux->format ('Y-m-d'));
        $Pedido -> usuario_id = $request->usuario_id;
        $Pedido -> save();

        $Plato_pedido = new Plato_pedido();
        $Plato_pedido -> cantidad = $request->cantidad;
        $Plato_pedido -> plato_id = $request->plato_id;
        $Plato_pedido -> pedido_id = $Pedido->pedido_id;
        $Plato_pedido -> createdAt = Date($aux->format ('Y-m-d'));
        $Plato_pedido -> updatedAt = Date($aux->format ('Y-m-d'));

        $Plato_pedido -> save();
        
    	$info = 'Pedido creado correctacmente';
    	$status = true;

    	return(ResponseBuilder::result($status, $info, $Pedido));
    }   

    
}