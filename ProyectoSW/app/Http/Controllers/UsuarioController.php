<?php

namespace App\Http\Controllers;

use App\Usuario;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class UsuarioController extends BaseController
{
    public function all(Request $request){
    	$Usuario = Usuario::all();
    	return response()->json($Usuario, 200);    	
    }

    public function getUsuario(Request $request, $correo){
    	if($request -> isJson()){
    		$Usuario = Usuario::where('correo', $correo) -> get();
    		if(!$Usuario -> isEmpty()){
    			$status = true;
    			$info = "Data is listed successfuly";
    		}
    		else{
 		   		$status = false;
    			$info = "Data is not listed successfuly";	
    		}
    		return ResponseBuilder::result($status, $info, $Usuario);
    	}
    	else{
    		$status = false;
    		$info = "Unauthorized";	
    		return ResponseBuilder::result($status, $info);
    	}    	
    }

    public function createUsuario(Request $request){

    	$Usuario = new Usuario();
    	$aux = new DateTime('now');
    	$Usuario -> nombre = $request->nombre;
        $Usuario -> correo = $request->correo;
        $Usuario -> numero_telefono = $request->numero_telefono;
        $Usuario -> password = $request->password;
        $Usuario -> estado = $request->estado;
        $Usuario -> createdAt = Date($aux->format ('Y-m-d'));
        $Usuario -> updatedAt = Date($aux->format ('Y-m-d'));

        $Usuario -> save();
        
    	$info = 'Usuario creado correctacmente';
    	$status = true;

    	return(ResponseBuilder::result($status, $info, $Usuario));
    }   

    
}