<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Restaurante extends Model
{
    
    protected $table = 'modelos_restaurante';
    protected $primaryKey = 'restaurante_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurante_id', 'nombre', 'horario', 'calificacion', 'estado', 'createdAt', 'updatedAt'
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
