<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router -> group(['prefix' => 'usuario'], function($router){
    $router->get('', 'UsuarioController@all');

    $router->post('', 'UsuarioController@createUsuario');
});

$router -> group(['prefix' => 'restaurante'], function($router){
    $router->get('', 'RestauranteController@all');

    $router->post('', 'RestauranteController@createRestaurante');
});

$router -> group(['prefix' => 'pedido'], function($router){
    $router->get('', 'PedidoController@all');

    $router->post('', 'PedidoController@createPedido');
});

$router -> group(['prefix' => 'platos'], function($router){
    $router->get('', 'PlatosController@all');    
});